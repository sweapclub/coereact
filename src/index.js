import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import 'antd/dist/antd.css'; // it will use all of project
import 'antd/lib/modal/style/css';
import 'antd/lib/tabs/style/css';
import Hello from './hello/Hello';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Hello />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

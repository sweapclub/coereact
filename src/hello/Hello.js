import React from 'react';
import { Modal, Button } from 'antd';
import { Tabs } from 'antd';
import axios from 'axios';

import './Hello.css';

// function Hello() {
//     return (<p1>My name is Nut</p1>);
// }
const TabPane = Tabs.TabPane;

class Hello extends React.Component {

    constructor(props) {

        super(props);
        this.state = {
            date: new Date(),
            name: 'Nuttasak',
            counter: 0,
            visible: false,
            data: []
        }


        this.onClick = this.onClick.bind(this)
    }

    callback(key) {
        console.log(key);
    }

    // API
    country = () => {
        return axios.get('http://dataapi.moc.go.th/summary-countries?year=2017&month=12&limit=2')
            .then(res => {
                this.setState({ data: res.data });
                console.log(this.state.data);
            });
    }

    componentDidMount() {
        this.country();
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };



    render() {
        return (
            <div>

                <Tabs defaultActiveKey="1" onChange={this.callback}>
                    <TabPane tab="Tab 1" key="1">
                        <h1>DIO : Za Warudo !!!</h1>
                        <Greeting name={this.state.name} />
                        <p>Click Time : {this.state.counter}</p>
                        <button onClick={this.onClick}>ADD MORE !</button>

                    </TabPane>
                    <TabPane tab="Tab 2" key="2">

                        <Button type="primary" onClick={this.showModal}>
                            Open Modal
                </Button>
                        <Modal
                            title="Basic Modal"
                            visible={this.state.visible}
                            onOk={this.handleOk}
                            onCancel={this.handleCancel}
                        >
                            <p>Some contents...</p>
                            <p>Some contents...</p>
                            <p>Some contents...</p>
                        </Modal>
                    </TabPane>
                    <TabPane tab="Tab 3" key="3">

                        {this.state.data.map(res => <p>{res.country_name_en}</p>)}
                    </TabPane>

                </Tabs>
            </div>
        )
    }

    onClick() {
        this.setState({ counter: this.state.counter + 1 })
    }
}

const Greeting = (props) => {
    return (<p>Hello {props.name} !</p>)
}

export default Hello;